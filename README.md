# TLV Parser

This is an Android Application to decode a TLV message and show tag, tag meaning and value for each tlv component.

The project was built with the pattern MVVM using the following features:

- [Android Architecture Components](https://developer.android.com/topic/libraries/architecture/)
- [Dependency injection with Dagger](https://google.github.io/dagger/)
- [BER TLV library to decode TLV Messages](https://github.com/binaryfoo/emv-bertlv)



