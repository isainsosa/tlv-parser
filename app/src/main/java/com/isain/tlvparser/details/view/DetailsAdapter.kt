package com.isain.tlvparser.details.view

import android.support.v4.view.animation.LinearOutSlowInInterpolator
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.*
import com.isain.tlvparser.R
import com.isain.tlvparser.database.entity.DecodedDataEntity
import com.isain.tlvparser.databinding.DecodedDataListItemBinding

class DetailsAdapter(private val list: RecyclerView, private val animate: Boolean): RecyclerView.Adapter<DetailsAdapter.ViewHolder>() {

    private val data: MutableList<DecodedDataEntity> = arrayListOf()

    fun setDecodedData(decodedData: List<DecodedDataEntity>?) {
        data.clear()
        decodedData?.let {
            data.addAll(it)
            if (animate) {
                setUpListAnimation()
            }
        }
        notifyDataSetChanged()
    }

    private fun setUpListAnimation() {
        val animationSet = AnimationSet(true).apply {
            duration = 400
            interpolator = LinearOutSlowInInterpolator()
            addAnimation(AlphaAnimation(0f, 1f))
            addAnimation(TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
                    Animation.RELATIVE_TO_PARENT, 0.25f, Animation.RELATIVE_TO_SELF, 0.0f))
        }
        list.layoutAnimation = LayoutAnimationController(animationSet, 0.15f)
    }

    open class ViewHolder(rootView: View): RecyclerView.ViewHolder(rootView) {
        open fun bind(decodedDataEntity: DecodedDataEntity) {}
    }

    override fun onCreateViewHolder(parent: ViewGroup, headerViewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return DecodedDataItemViewHolder(DecodedDataListItemBinding.inflate(layoutInflater, parent, false))
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bind(data[position])
    }

    private inner class DecodedDataItemViewHolder internal constructor(
        private val binding: DecodedDataListItemBinding): ViewHolder(binding.root) {

        override fun bind(decodedDataEntity: DecodedDataEntity) {
            super.bind(decodedDataEntity)
            val context = binding.root.context
            binding.tag.text = String.format(context.getString(R.string.tag_and_tag_meaning), decodedDataEntity.tag,
                if (decodedDataEntity.tagMeaning == "?") context.getString(R.string.tlv_unknown_tag)
                else decodedDataEntity.tagMeaning)
            binding.value.text = decodedDataEntity.value
        }
    }

    override fun getItemCount(): Int = data.size
}