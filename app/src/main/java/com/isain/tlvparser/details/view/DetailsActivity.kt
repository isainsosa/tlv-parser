package com.isain.tlvparser.details.view

import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.isain.tlvparser.R
import com.isain.tlvparser.core.base.BaseActivity
import com.isain.tlvparser.core.resource.Status
import com.isain.tlvparser.details.viewmodel.DetailsActivityViewModel

import kotlinx.android.synthetic.main.activity_details.*
import kotlinx.android.synthetic.main.content_details.*

class DetailsActivity : BaseActivity() {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, DetailsActivity::class.java)
        }
    }

    private val viewModel: DetailsActivityViewModel by lazy { getViewModel(DetailsActivityViewModel::class.java) }
    private lateinit var adapter: DetailsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
        }
        if (savedInstanceState == null) {
            viewModel.startRetrieveDecodedData(true)
        }

        adapter = DetailsAdapter(list, true)
        list.adapter = adapter

        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.decodedData().observe(this, Observer {
            @Suppress("NON_EXHAUSTIVE_WHEN") when(it?.status) {
                Status.SUCCESS -> {
                    val decodedData = it.data
                    decodedData?.map {
                        println("TLV -> ${it.tag}")
                    }
                    adapter.setDecodedData(decodedData)
                }
                Status.ERROR -> {
                    Toast.makeText(this, getString(R.string.retrieve_saved_decoded_data_error), Toast.LENGTH_LONG).show()
                }
            }
        })
    }
}
