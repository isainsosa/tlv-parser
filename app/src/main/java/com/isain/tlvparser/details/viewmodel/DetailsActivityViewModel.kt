package com.isain.tlvparser.details.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import com.isain.tlvparser.core.resource.LiveDataResource
import com.isain.tlvparser.database.entity.DecodedDataEntity
import com.isain.tlvparser.tlvdata.business.AppDecodedDataRepository
import javax.inject.Inject

class DetailsActivityViewModel @Inject constructor(private val decodedDataRepository: AppDecodedDataRepository) : ViewModel() {
    //Retrieve saved decoded data region
    private val retrieveDecodedData: MutableLiveData<Boolean> = MutableLiveData()
    private val decodedData: LiveDataResource<List<DecodedDataEntity>> = Transformations.switchMap(retrieveDecodedData) {
        if (it.let { it } == true) {
            decodedDataRepository.getAllDecodedData()
        } else {
            MutableLiveData()
        }
    }
    //Endregion

    fun startRetrieveDecodedData(retrieve: Boolean) {
        retrieveDecodedData.value = retrieve
    }

    fun decodedData() = this.decodedData
}