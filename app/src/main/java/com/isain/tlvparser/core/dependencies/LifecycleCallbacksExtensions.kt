package com.isain.tlvparser.core.dependencies

import android.app.Activity
import android.app.Application
import android.os.Bundle

fun Application.registerActivityCreateCallback(callback: (Activity) -> Unit) {
    registerActivityLifecycleCallbacks(object : Application.ActivityLifecycleCallbacks {
        override fun onActivityCreated(activity: Activity?, savedInstanceState: Bundle?) {
            activity?.let { callback.invoke(it) }
        }

        override fun onActivityPaused(activity: Activity?) {}
        override fun onActivityResumed(activity: Activity?) {}
        override fun onActivityStarted(activity: Activity?) {}
        override fun onActivityDestroyed(activity: Activity?) {}
        override fun onActivitySaveInstanceState(activity: Activity?, savedInstanceState: Bundle?) {}
        override fun onActivityStopped(activity: Activity?) {}
    })
}