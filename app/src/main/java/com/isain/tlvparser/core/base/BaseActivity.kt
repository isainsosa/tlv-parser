package com.isain.tlvparser.core.base

import android.annotation.SuppressLint
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import com.isain.tlvparser.core.dependencies.Injectable
import javax.inject.Inject

@SuppressLint("Registered")
open class BaseActivity: AppCompatActivity(), Injectable {

    @Inject
    internal lateinit var viewModelFactory: ViewModelProvider.Factory

    fun <VM : ViewModel> getViewModel(modelClass: Class<VM>): VM =
        ViewModelProviders.of(this, viewModelFactory).get(modelClass)
}