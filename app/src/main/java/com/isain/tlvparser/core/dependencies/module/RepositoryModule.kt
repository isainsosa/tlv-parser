package com.isain.tlvparser.core.dependencies.module

import com.isain.tlvparser.database.AppCacheDatabaseRepository
import com.isain.tlvparser.database.CacheDatabaseRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun provideCacheDataBaseRepository(appCacheDatabaseRepository: AppCacheDatabaseRepository):
        CacheDatabaseRepository = appCacheDatabaseRepository
}