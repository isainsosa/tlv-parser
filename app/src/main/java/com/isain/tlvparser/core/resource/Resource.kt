package com.isain.tlvparser.core.resource

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer

data class Resource<out DataType>(val status: Status, val message: String? = null, val errorCode: Int = -1,
    val data: DataType? = null) {

    companion object {
        fun <DataType> loading(data: DataType? = null): Resource<DataType> = Resource(Status.LOADING ,data = data)

        fun <DataType> success(data: DataType? = null): Resource<DataType> = Resource(Status.SUCCESS ,data = data)

        fun <DataType> error(message: String? = null, errorCode: Int = -1): Resource<DataType> =
                Resource(Status.ERROR , message, errorCode)
    }

    fun isSuccess(): Boolean = status == Status.SUCCESS
    fun isError(): Boolean = status == Status.ERROR
}

enum class Status {
    LOADING,
    SUCCESS,
    ERROR
}

typealias LiveDataResource<DataType> = LiveData<Resource<DataType>>

typealias MutableLiveDataResource<DataType> = MutableLiveData<Resource<DataType>>

typealias MediatorLiveDataResource<DataType> = MediatorLiveData<Resource<DataType>>

typealias ObserverResource<DataType> = Observer<Resource<DataType>>