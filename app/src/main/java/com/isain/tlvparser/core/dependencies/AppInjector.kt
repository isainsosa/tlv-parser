package com.isain.tlvparser.core.dependencies

import com.isain.tlvparser.CustomApplication
import com.isain.tlvparser.core.dependencies.component.DaggerApplicationComponent
import dagger.android.AndroidInjection

object AppInjector {
    fun init(customApplication: CustomApplication) {
        DaggerApplicationComponent.builder().application(customApplication).build().inject(customApplication)
        customApplication.registerActivityCreateCallback { activity ->
            if (activity is Injectable) {
                AndroidInjection.inject(activity)
            }
        }
    }
}