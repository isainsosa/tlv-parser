package com.isain.tlvparser.core.dependencies.module;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import com.isain.tlvparser.details.viewmodel.DetailsActivityViewModel;
import com.isain.tlvparser.home.viewmodel.MainActivityViewModel;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@SuppressWarnings("unused")
@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainActivityViewModel.class)
    abstract ViewModel bindMainActivityViewModel(MainActivityViewModel mainActivityViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(DetailsActivityViewModel.class)
    abstract ViewModel bindDetailsActivityViewModel(DetailsActivityViewModel detailsActivityViewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);
}
