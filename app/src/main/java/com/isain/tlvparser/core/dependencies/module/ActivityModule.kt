package com.isain.tlvparser.core.dependencies.module

import com.isain.tlvparser.details.view.DetailsActivity
import com.isain.tlvparser.home.view.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
internal abstract class ActivityModule {
    @ContributesAndroidInjector()
    internal abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector()
    internal abstract fun contributeDetailsActivity(): DetailsActivity
}