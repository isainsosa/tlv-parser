package com.isain.tlvparser.core.dependencies.module

import com.isain.tlvparser.tlvdata.business.AppTLVDecoder
import com.isain.tlvparser.tlvdata.business.TLVDecoder
import com.isain.tlvparser.tlvdata.business.AppDecodedDataRepository
import com.isain.tlvparser.tlvdata.business.DecodedDataRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class BusinessModule {

    @Provides
    @Singleton
    fun provideTLVDecoder(appTLVDecoder: AppTLVDecoder): TLVDecoder = appTLVDecoder

    @Provides
    @Singleton
    fun provideDecodedDataRepository(appDecodedDataRepository: AppDecodedDataRepository):
        DecodedDataRepository = appDecodedDataRepository
}