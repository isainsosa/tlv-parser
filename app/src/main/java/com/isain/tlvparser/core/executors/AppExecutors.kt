package com.isain.tlvparser.core.executors

import android.os.Handler
import android.os.Looper
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import javax.inject.Singleton

/**
 * Global executor pools for the whole application.
 *
 * Grouping tasks like this avoids the effects of task starvation (e.g. disk reads don't wait behind
 * webservice requests).
 */
@Singleton
class AppExecutors(private val diskIO: Executor = Executors.newSingleThreadExecutor(),
                   private val networkIO: Executor = Executors.newFixedThreadPool(3),
                   private val mainThread: Executor = AppExecutors.MainThreadExecutor()) {

    fun diskIO(f: () -> Unit) {
        diskIO.execute(f)
    }

    fun networkIO(): Executor = networkIO

    fun networkIO(f: () -> Unit) {
        networkIO.execute(f)
    }

    fun mainThread(): Executor = mainThread

    fun mainThread(f: () -> Unit) {
        mainThread.execute(f)
    }

    private class MainThreadExecutor : Executor {

        private val mainThreadHandler = Handler(Looper.getMainLooper())

        override fun execute(command: Runnable?) {
            command?.let { mainThreadHandler.post(it) }
        }
    }
}