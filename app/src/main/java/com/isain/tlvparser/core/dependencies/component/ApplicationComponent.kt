package com.isain.tlvparser.core.dependencies.component

import android.app.Application
import com.isain.tlvparser.CustomApplication
import com.isain.tlvparser.core.dependencies.module.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    (AndroidInjectionModule::class),
    (ApplicationModule::class),
    (ActivityModule::class),
    (BusinessModule::class),
    (ViewModelModule::class),
    (DataBaseModule::class),
    (RepositoryModule::class)
])

interface ApplicationComponent {
    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ApplicationComponent
    }

    fun inject(customApplication: CustomApplication)
}