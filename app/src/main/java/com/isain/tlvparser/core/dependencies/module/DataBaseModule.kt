package com.isain.tlvparser.core.dependencies.module

import android.app.Application
import android.arch.persistence.room.Room
import com.isain.tlvparser.database.CacheDatabase
import com.isain.tlvparser.database.dao.CacheDatabaseDao
import com.isain.tlvparser.database.dao.DecodedDataDao
import dagger.Module
import dagger.Provides
import dagger.Reusable
import javax.inject.Singleton

@Module
class DataBaseModule {

    @Provides
    @Singleton
    fun provideCacheDatabase(application: Application): CacheDatabase =
        Room.databaseBuilder(application.applicationContext, CacheDatabase::class.java,
            "CACHE").fallbackToDestructiveMigration().build()

    @Provides
    @Reusable
    fun provideCacheDatabaseDao(database: CacheDatabase): CacheDatabaseDao = database.cacheDataBaseDao()

    @Provides
    @Reusable
    fun provideDecodedDataDao(database: CacheDatabase): DecodedDataDao = database.decodedDataDao()
}