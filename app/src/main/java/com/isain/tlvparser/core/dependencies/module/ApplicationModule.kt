package com.isain.tlvparser.core.dependencies.module

import android.app.Application
import android.content.Context
import com.isain.tlvparser.core.executors.AppExecutors
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
internal class ApplicationModule {

    @Provides
    @Singleton
    fun provideAppExecutors(): AppExecutors = AppExecutors()

    @Provides
    @Singleton
    fun provideAppContext(application: Application): Context = application.baseContext
}