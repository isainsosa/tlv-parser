package com.isain.tlvparser.tlvdata.business

import com.isain.tlvparser.core.resource.LiveDataResource
import io.github.binaryfoo.DecodedData

interface TLVDecoder {
    fun decodeTLVMessage(message: String): LiveDataResource<List<DecodedData>>
}