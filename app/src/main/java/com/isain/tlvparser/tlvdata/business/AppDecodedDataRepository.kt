package com.isain.tlvparser.tlvdata.business

import android.arch.lifecycle.MediatorLiveData
import com.isain.tlvparser.core.executors.AppExecutors
import com.isain.tlvparser.core.resource.LiveDataResource
import com.isain.tlvparser.core.resource.MediatorLiveDataResource
import com.isain.tlvparser.core.resource.Resource
import com.isain.tlvparser.database.dao.DecodedDataDao
import com.isain.tlvparser.database.entity.DecodedDataEntity
import javax.inject.Inject

class AppDecodedDataRepository @Inject constructor(private val appExecutors: AppExecutors,
    private val decodedDataDao: DecodedDataDao): DecodedDataRepository {

    override fun saveDecodedData(decodedData: DecodedDataEntity) {
        appExecutors.diskIO {
            decodedDataDao.saveDecodedData(decodedData)
        }
    }

    override fun deleteAllDecodedData() {
        appExecutors.diskIO {
            decodedDataDao.deleteAllDecodedData()
        }
    }

    override fun getAllDecodedData(): LiveDataResource<List<DecodedDataEntity>> {
        val result: MediatorLiveDataResource<List<DecodedDataEntity>> = MediatorLiveData()
        result.value = Resource.loading()
        appExecutors.diskIO {
            val decodedDataSource = decodedDataDao.getDecodedData()
            result.addSource(decodedDataSource) {
                result.postValue(Resource.success(it))
            }
        }

        return result
    }
}