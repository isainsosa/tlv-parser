package com.isain.tlvparser.tlvdata.business

import android.arch.lifecycle.MediatorLiveData
import android.content.Context
import com.isain.tlvparser.R
import com.isain.tlvparser.core.executors.AppExecutors
import com.isain.tlvparser.core.resource.LiveDataResource
import com.isain.tlvparser.core.resource.MediatorLiveDataResource
import com.isain.tlvparser.core.resource.Resource
import com.isain.tlvparser.database.entity.DecodedDataEntity
import io.github.binaryfoo.DecodedData
import io.github.binaryfoo.RootDecoder
import java.util.*
import javax.inject.Inject

class AppTLVDecoder @Inject constructor(private val appExecutors: AppExecutors,
    private val appDecodedDataRepository: AppDecodedDataRepository,
    private val context: Context
): TLVDecoder {

    companion object {
        const val meta: String = "EMV"
        const val tag: String = "constructed"
    }

    override fun decodeTLVMessage(message: String): LiveDataResource<List<DecodedData>> {
        val result: MediatorLiveDataResource<List<DecodedData>> = MediatorLiveData()
        result.value = Resource.loading()
        appExecutors.diskIO {
            try {
                val decodedData: List<DecodedData> = RootDecoder().decode(message, meta, tag)
                if (decodedData.isEmpty().not()) {
                    appDecodedDataRepository.deleteAllDecodedData()
                    decodedData.map {
                        val tagMeaning = it.rawData.substring(it.rawData.indexOf("(").inc(),
                                it.rawData.indexOf(")")).capitalize()

                        val tagValue = it.fullDecodedData.replace("{NULL}", "-")

                        appDecodedDataRepository.saveDecodedData(DecodedDataEntity(UUID.randomUUID().toString(),
                                it.tag.toString(), tagMeaning, tagValue))
                    }
                    result.postValue(Resource.success(decodedData))
                } else {
                    result.postValue(Resource.error(context.getString(R.string.tlv_message_not_valid)))
                }
            } catch (ex: Exception) {
                result.postValue(Resource.error(context.getString(R.string.tlv_message_not_valid)))
            }
        }
        return result
    }
}