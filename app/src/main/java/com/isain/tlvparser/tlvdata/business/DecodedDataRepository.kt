package com.isain.tlvparser.tlvdata.business

import com.isain.tlvparser.core.resource.LiveDataResource
import com.isain.tlvparser.database.entity.DecodedDataEntity

interface DecodedDataRepository {

    fun saveDecodedData(decodedData: DecodedDataEntity)

    fun deleteAllDecodedData()

    fun getAllDecodedData(): LiveDataResource<List<DecodedDataEntity>>
}