package com.isain.tlvparser.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import android.arch.persistence.room.Transaction

@Dao
abstract class CacheDatabaseDao {

    @Query("DELETE FROM decoded_data")
    abstract fun deleteAllFromDecodedData()

    @Transaction
    open fun deleteAll() {
        deleteAllFromDecodedData()
    }
}