package com.isain.tlvparser.database.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "decoded_data")
data class DecodedDataEntity(@PrimaryKey @ColumnInfo(name = "data_id") val dataId: String,
    val tag: String, val tagMeaning: String, val value: String)