package com.isain.tlvparser.database.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.isain.tlvparser.database.entity.DecodedDataEntity

@Dao
interface DecodedDataDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveDecodedData(decodedData: DecodedDataEntity)

    @Query("DELETE FROM decoded_data")
    fun deleteAllDecodedData()

    @Query("SELECT * FROM decoded_data")
    fun getDecodedData(): LiveData<List<DecodedDataEntity>>
}