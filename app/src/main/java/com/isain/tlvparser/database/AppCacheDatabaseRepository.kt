package com.isain.tlvparser.database

import android.arch.lifecycle.MediatorLiveData
import com.isain.tlvparser.core.executors.AppExecutors
import com.isain.tlvparser.core.resource.MediatorLiveDataResource
import com.isain.tlvparser.core.resource.Resource
import com.isain.tlvparser.database.dao.CacheDatabaseDao
import javax.inject.Inject

class AppCacheDatabaseRepository @Inject constructor(private val appExecutors: AppExecutors,
    private val cacheDatabaseDao: CacheDatabaseDao): CacheDatabaseRepository {

    override fun clearDatabase(isAppInit: Boolean): MediatorLiveDataResource<Unit> {
        val result: MediatorLiveDataResource<Unit> = MediatorLiveData()
        if (isAppInit) {
            result.value = Resource.loading()
            appExecutors.diskIO {
                cacheDatabaseDao.deleteAll()
                result.postValue(Resource.success())
            }
        } else {
            result.value = Resource.success()
        }
        return result
    }
}