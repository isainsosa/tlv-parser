package com.isain.tlvparser.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.isain.tlvparser.database.dao.CacheDatabaseDao
import com.isain.tlvparser.database.dao.DecodedDataDao
import com.isain.tlvparser.database.entity.DecodedDataEntity

@Database(entities = [(DecodedDataEntity::class)], version = 1)
abstract class CacheDatabase: RoomDatabase() {

    abstract fun cacheDataBaseDao(): CacheDatabaseDao

    abstract fun decodedDataDao(): DecodedDataDao
}