package com.isain.tlvparser.database

import com.isain.tlvparser.core.resource.MediatorLiveDataResource

interface CacheDatabaseRepository {

    fun clearDatabase(isAppInit: Boolean): MediatorLiveDataResource<Unit>
}