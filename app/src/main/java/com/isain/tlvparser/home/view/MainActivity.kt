package com.isain.tlvparser.home.view

import android.app.ProgressDialog
import android.arch.lifecycle.Observer
import android.os.Bundle
import android.widget.Toast
import com.isain.tlvparser.R
import com.isain.tlvparser.core.base.BaseActivity
import com.isain.tlvparser.core.resource.Status
import com.isain.tlvparser.details.view.DetailsActivity
import com.isain.tlvparser.home.viewmodel.MainActivityViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.contente_main.*

class MainActivity : BaseActivity() {

    private val viewModel: MainActivityViewModel by lazy { getViewModel(MainActivityViewModel::class.java) }
    private lateinit var progressDialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        viewModel.setIsAppInit(savedInstanceState == null)

        confirmBtn.setOnClickListener {
            viewModel.decodeMessage(input_text.text.toString())
        }

        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.decodedData().observe(this, Observer {
            @Suppress("NON_EXHAUSTIVE_WHEN") when(it?.status) {
                Status.LOADING -> {
                    showLoading()
                }
                Status.SUCCESS -> {
                    hideLoading()
                    goToDetailsActivity()
                }
                Status.ERROR -> {
                    hideLoading()
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    private fun goToDetailsActivity() {
        startActivity(DetailsActivity.newIntent(this))
    }

    private fun showLoading() {
        progressDialog = ProgressDialog.show(this, "", getString(R.string.loading), false)
    }

    private fun hideLoading() {
        progressDialog.hide()
    }
}
