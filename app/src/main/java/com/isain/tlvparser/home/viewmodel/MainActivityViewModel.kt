package com.isain.tlvparser.home.viewmodel

import android.arch.lifecycle.*
import com.isain.tlvparser.core.resource.LiveDataResource
import com.isain.tlvparser.core.resource.Status
import com.isain.tlvparser.database.CacheDatabaseRepository
import com.isain.tlvparser.tlvdata.business.TLVDecoder
import io.github.binaryfoo.DecodedData
import javax.inject.Inject

class MainActivityViewModel @Inject constructor(private val cacheDatabaseRepository: CacheDatabaseRepository,
    private val tlvDecoder: TLVDecoder) : ViewModel() {

    //Clear database region
    private val mediatorLiveData: MediatorLiveData<Unit> = MediatorLiveData()
    private var isAppInit: MutableLiveData<Boolean> = MutableLiveData()
    private val clearDatabase: LiveDataResource<Unit> = Transformations.switchMap(isAppInit) {
        cacheDatabaseRepository.clearDatabase(it)
    }
    private val observer: Observer<Unit> = Observer { }
    //Endregion

    //Decode TLV Message region
    private val messageToDecode: MutableLiveData<String?> = MutableLiveData()
    private val decodedData: LiveDataResource<List<DecodedData>> = Transformations.switchMap(messageToDecode) { message ->
        if (message != null) {
            tlvDecoder.decodeTLVMessage(message)
        } else {
            MutableLiveData()
        }
    }
    //Endregion

    init {
        mediatorLiveData.addSource(clearDatabase) clearDatabaseSource@{ clearDatabaseResult ->
            if (clearDatabaseResult?.status != Status.SUCCESS) {
                return@clearDatabaseSource
            }
            mediatorLiveData.removeSource(clearDatabase)
        }

        mediatorLiveData.observeForever(observer)
    }

    override fun onCleared() {
        super.onCleared()
        mediatorLiveData.removeObserver(observer)
    }

    fun decodeMessage(message: String) {
        messageToDecode.value = message
    }

    fun decodedData() = this.decodedData

    fun setIsAppInit(isAppInit: Boolean) {
        this.isAppInit.value = isAppInit
    }
}