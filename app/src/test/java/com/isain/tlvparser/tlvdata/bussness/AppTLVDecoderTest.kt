package com.isain.tlvparser.tlvdata.bussness

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import android.content.Context
import com.isain.tlvparser.core.executors.AppExecutors
import com.isain.tlvparser.core.resource.Resource
import com.isain.tlvparser.database.dao.DecodedDataDao
import com.isain.tlvparser.tlvdata.business.AppDecodedDataRepository
import com.isain.tlvparser.tlvdata.business.AppTLVDecoder
import com.nhaarman.mockito_kotlin.mock
import io.github.binaryfoo.DecodedData
import org.junit.Test
import org.junit.Before
import org.junit.Rule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.exceptions.base.MockitoException
import org.mockito.junit.MockitoJUnitRunner
import java.util.concurrent.Executor

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(MockitoJUnitRunner::class)
class AppTLVDecoderTest {

    @Suppress("unused")
    @get:Rule
    var instantTask = InstantTaskExecutorRule()

    @Mock
    private lateinit var decodedDataDao: DecodedDataDao
    @Mock
    private lateinit var context: Context

    private lateinit var appExecutors: AppExecutors
    private lateinit var appDecodedDataRepository: AppDecodedDataRepository
    private lateinit var appTLVDecoder: AppTLVDecoder

    private val tlvMessage = "5F201A54444320424C41434B20554E4C494D49544544205649534120204F07A0000000" +
            "0310105F24032307319F160F4243544553542031323334353637389F21031826509A031406179F020600000" +
            "00000019F03060000000000009F34030203009F120C56495341204352454449544F9F0607A0000000031010" +
            "5F300202019F4E0F616263640000000000000000000000C408491573FFFFFF1097C00A09117101800165E00" +
            "00AC2820168D9DE289AAD770BE408F6B1D4E0A2576CEA7F03CD479CE3A1827375D6C4D4959ACDB5D3B6F84C" +
            "D83430F4346C35E48A77A0D5F36FBEA444C2D8701C07FFC7AF06C0485D68F7A83FC30840D3C0766EC4EE669" +
            "BE5A42BAD4C7459680FAAAE9C4EFEFFEB5A590E53B3E91B3CD28A415C2C9484E26DA5A15592BBCD1F45CF49" +
            "D27A9D480B031957DF8C790C55FF60DB192CCD070FA4F7BCDC99E7F7567C2F991B5536F9336BA66D68115D5" +
            "4BC3642A9CA47FDD162FCDC33E455AAC283975DACC98CBE9A6611E996F0740072CF8E32D3D9F39F4BB25568" +
            "F5CC3E7F5DE158E4D62BF4E7185CF13BD068C4F062C26A3BBF88E056F249130E89AA29E52A1EBB6BAD98296" +
            "822F10949F0C825D1449DA7EF4431AB846D0DDB916F2901359DD9A3B3395BAC9F9BE4D24657F65B030DDADA" +
            "53577A14D9F5F776B6FF7EAB99D8C4BB08BEF2016C72D94B1DB91BCF0238405B7857646DCE5F79871D96B6A" +
            "6652090FD8CFCC59973433919A6D0533DFE"

    @Before
    fun setup() {
        appExecutors = AppExecutors(Executor { it.run() }, Executor { it.run() }, Executor { it.run() })
        appDecodedDataRepository = AppDecodedDataRepository(appExecutors, decodedDataDao)
        appTLVDecoder = AppTLVDecoder(appExecutors, appDecodedDataRepository, context)
    }

    @Test
    @Throws(MockitoException::class)
    fun `verify that TLV Message is decoded correctly`() {
        appTLVDecoder.decodeTLVMessage(tlvMessage)
        val observer = mock<Observer<Resource<List<DecodedData>>>>()
        val result = appTLVDecoder.decodeTLVMessage(tlvMessage)
        result.observeForever(observer)
        result.value?.data?.map {
            println("Tag: ${it.rawData}, Value: ${it.fullDecodedData}")
        }
        result.removeObserver(observer)
    }
}
